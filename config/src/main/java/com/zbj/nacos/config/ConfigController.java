package com.zbj.nacos.config;

import com.alibaba.nacos.api.config.annotation.NacosConfigListener;
import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ConfigController
 *
 * @author weigang
 * @create 2019-07-27
 **/
@RestController
@RequestMapping("/config")
public class ConfigController {

    @NacosValue(value = "${useLocalCache:false}", autoRefreshed = true)
    private boolean useLocalCache;

    @NacosValue(value = "${zbj.demo:1}", autoRefreshed = true)
    private String zbjDemo;

    @GetMapping("/get")
    public boolean get() {
        return useLocalCache;
    }

    @GetMapping("/demo")
    public String demo() {
        return zbjDemo;
    }

    @NacosConfigListener(dataId = "zbj.demo")
    public void zbjDemo(String message){
        System.out.println(message);
        zbjDemo = message;
    }
}