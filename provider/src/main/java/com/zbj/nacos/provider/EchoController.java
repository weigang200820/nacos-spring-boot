package com.zbj.nacos.provider;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * EchoController
 *
 * @author weigang
 * @create 2019-07-25
 **/
@RestController
@RequestMapping("/echo")
public class EchoController {

    @RequestMapping(value = "/{echo}", method = RequestMethod.GET)
    public String echo(@PathVariable String echo) {
        return "Hello Nacos discovery-> " + echo;
    }
}